#include "aam.h"

aam::aam(){
  m_num_alarms=0;
}

void aam::add_alarm(alarm * _alarm){
  if(m_num_alarms>=AAM_MAX_ALARMS){return;}
  m_alarms[m_num_alarms]=_alarm;
  m_num_alarms++;
}

void aam::init(){
  for(int i=0;i<m_num_alarms;i++){m_alarms[i]->init();}
}

void aam::loop(){

  for(int i=0;i<m_num_alarms;i++){
    bool was_triggered=m_alarms[i]->is_triggered();
    if(m_alarms[i]->eval() and !was_triggered){m_alarms[i]->trigger();}
  }

}
