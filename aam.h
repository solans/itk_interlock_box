#ifndef AAM
#define AAM

#include <Arduino.h>
#include "alarm.h"

#define AAM_MAX_ALARMS 10

class aam{

public:
  
  aam();
  void add_alarm(alarm * _alarm);
  void init();
  void loop();
  
private:

  alarm * m_alarms[AAM_MAX_ALARMS];
  int m_num_alarms;

};
#endif
