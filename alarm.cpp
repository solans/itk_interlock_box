#include "alarm.h"

alarm::alarm(String name){
  m_name = name;
  m_triggered = false;
  m_num_inputs = 0;
  m_num_outputs = 0;
}

String alarm::get_name(){
  return m_name;
}

void alarm::add_input(input *_input){
  if(m_num_inputs>ALARM_MAX_INPUTS){return;}
  m_inputs[m_num_inputs]=_input;
  m_num_inputs++;
}

void alarm::add_output(output *_output){
  if(m_num_outputs>ALARM_MAX_OUTPUTS){return;}
  m_outputs[m_num_outputs]=_output;
  m_num_outputs++;
}

bool alarm::init(){
  for(int i=0; i<m_num_inputs; i++){m_inputs[i]->init();}
  for(int i=0; i<m_num_outputs; i++){m_outputs[i]->init();}
}

bool alarm::is_triggered(){
  return m_triggered;
}

void alarm::acknowledge(){
  m_triggered=false;
}

bool alarm::eval(){
  for(int i=0; i<m_num_inputs; i++){if(m_inputs[i]->eval()){m_triggered=true;}}
  if(m_triggered){Serial.println("ALARM conditions met: "+m_name);}
  return m_triggered;
}

void alarm::trigger(){
  m_triggered=true;
  Serial.println("ALARM triggered: "+m_name);
  for(int i=0; i<m_num_outputs; i++){m_outputs[i]->trigger();}
}
