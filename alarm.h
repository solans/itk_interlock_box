#ifndef ALARM
#define ALARM

#include <Arduino.h>
#include "input.h"
#include "output.h"

#define ALARM_MAX_INPUTS 10
#define ALARM_MAX_OUTPUTS 10

class alarm{

public:
  
  alarm(String name);
  String get_name();
  void add_input(input * _input);
  void add_output(output * _output);
  bool init();
  bool eval();
  bool is_triggered();
  void acknowledge();
  void trigger();
  
private:

  String m_name;
  input * m_inputs[ALARM_MAX_INPUTS];
  int m_num_inputs;
  output * m_outputs[ALARM_MAX_OUTPUTS];
  int m_num_outputs;
  bool m_triggered;
  
};
#endif
