#include "digital_input.h"
#include <Arduino_MachineControl.h>
#include <Arduino.h>
using namespace machinecontrol;

digital_input::digital_input(String name, int chan, bool trigger_on_error){
    m_name = name;
    m_chan = chan;
    m_triggered = false;
    m_value = 1;
    m_trigger_on_error = trigger_on_error;
}

String digital_input::get_name(){
  return m_name;
}

void digital_input::init(){
  digital_inputs.pinMode(m_chan,(PinMode)0);
}

int digital_input::value(){
  m_value = digital_inputs.read(m_chan);
  Serial.println(m_name+": "+m_value);
  return m_value;
}

bool digital_input::eval(){
  value();
  m_triggered = (m_value==0 || (m_value==-1 && m_trigger_on_error));
  return m_triggered;
}
