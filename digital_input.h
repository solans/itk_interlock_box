#ifndef DIGITAL_INPUT
#define DIGITAL_INPUT

#include <Arduino.h>
#include "input.h"

class digital_input: public input{

public:
  
  digital_input(String name, int din, bool trigger_on_error);
  String get_name();
  int value();
  void init();
  bool eval();
  
private:
  String m_name;
  int m_chan;
  int m_value;
  bool m_triggered;
  bool m_trigger_on_error;
};
#endif
