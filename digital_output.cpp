#include "digital_output.h"
#include <Arduino_MachineControl.h>
using namespace machinecontrol;

digital_output::digital_output(String name, int chan): output(){
  m_name = name;
  m_chan = chan;
  m_triggered = false;
}

void digital_output::init(){
  m_triggered = false;
  eval();
}

String digital_output::get_name(){
  return m_name;
}

bool digital_output::is_triggered(){
  return m_triggered;
}

void digital_output::trigger(){
  Serial.println("Output enabled: "+m_name);
  m_triggered=true;
  eval();
}

void digital_output::acknowledge(){
  Serial.println("Output acknowledged: "+m_name);
  m_triggered = false;
  eval();
}

bool digital_output::eval(){  
  digital_outputs.set(m_chan,!m_triggered);
  return m_triggered;
}
