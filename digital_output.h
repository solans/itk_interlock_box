#ifndef DIGITAL_OUTPUT
#define DIGITAL_OUTPUT

#include <Arduino.h>
#include "output.h"

class digital_output: public output{

public:
  
  digital_output(String name, int chan);
  String get_name();
  bool is_triggered();
  void init();
  void trigger();
  void acknowledge();
  bool eval();
  
private:
  String m_name;
  int m_chan;
  bool m_triggered;
  bool m_acknowledged;
};
#endif
