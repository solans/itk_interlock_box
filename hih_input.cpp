#include "hih_input.h"
#include <Arduino_MachineControl.h>
#include <Arduino.h>
using namespace machinecontrol;

hih_input::hih_input(String name, int chan_in, int chan_out, float threshold, bool trigger_above){
  m_name = name;
  m_chan_in = chan_in;
  m_chan_out = chan_out;
  m_threshold = threshold;
  m_trigger_above = trigger_above;
  m_raw=0;
  m_value = 0;
  m_triggered = false;
}

String hih_input::get_name(){
  return m_name;
}

void hih_input::init(){
  analog_out.write(m_chan_out,5);
  analog_in.set0_10V(); //fixme: needs to be single channel setting
}

int hih_input::raw(){
  m_raw = analog_in.read(m_chan_in);
  return m_raw;  
}

float hih_input::value(){
  m_raw=raw();
  m_value = map(m_raw,0,0xFFFF,0,200);
  Serial.println(m_name+": "+m_value);
  return m_value;
}

bool hih_input::eval(){
  m_value=value();
  if(m_trigger_above && m_value > m_threshold){m_triggered=true;}
  else if(!m_trigger_above && m_value < m_threshold){m_triggered=true;}
  else{m_triggered=false;}
  return m_triggered;
}
