#ifndef HIH_INPUT
#define HIH_INPUT

#include <Arduino.h>
#include "input.h"

class hih_input: public input{

public:
  
  hih_input(String name, int chan_in, int chan_out, float threshold, bool trigger_above);
  String get_name();
  int raw();
  float value();
  void init();
  bool eval();
  
private:
  String m_name;
  int m_chan_in;
  int m_chan_out;
  float m_threshold;
  bool m_trigger_above;
  int m_raw;
  float m_value;
  bool m_triggered;
};
#endif
