#ifndef INPUT
#define INPUT

class input{

public:
  
  input(){};
  virtual ~input(){};
  virtual void init()=0;
  virtual bool eval()=0;
  
};
#endif
