/**
 * MOQCHI: Module QC Hardware Interlock
 * EP-ADE-TK
 * 
 * Carlos.Solans@cern.ch
 **/
#include <Arduino.h>
#include <Arduino_MachineControl.h>

#include "digital_input.h"
#include "digital_output.h"
#include "tc_input.h"
#include "hih_input.h"
#include "alarm.h"
#include "aam.h"

using namespace machinecontrol;

digital_input di_door("DI_DOOR",DIN_READ_CH_PIN_00,false);
tc_input tc_ambient_high("TC_AMBIENT_HIGH",0,40,true);
hih_input ai_humi("AI_HUMI",0,0,50,true);
digital_output do_lv("DO_LV",0);
digital_output do_hv("DO_HV",1);
alarm al_ambient_high("AL_AMBIENT_HIGH");
alarm al_door_open("AL_DOOR_OPEN");
alarm al_humi_high("AL_HUMI_HIGH");
aam m_aam;

void setup() {
  Serial.begin(9600);
  Wire.begin();

  al_ambient_high.add_input(&tc_ambient_high);
  al_ambient_high.add_output(&do_lv);
  al_ambient_high.add_output(&do_hv);
  m_aam.add_alarm(&al_ambient_high);

  al_door_open.add_input(&di_door);
  al_door_open.add_output(&do_lv);
  al_door_open.add_output(&do_hv);
  m_aam.add_alarm(&al_door_open);

  al_humi_high.add_input(&ai_humi);
  al_humi_high.add_output(&do_lv);
  al_humi_high.add_output(&do_hv);
  m_aam.add_alarm(&al_humi_high);

  digital_inputs.init();
  m_aam.init();

  /*
  analog_out.write(0, 5);
  analog_in.set0_10V();
  */
}

void loop() {

  int t0=millis();
  //di_door.eval();
  m_aam.loop();
  /*
  uint16_t aiv=analog_in.read(0);
  float rh=map(aiv,0,0xFFFF,0,200);
  Serial.println("AI_0: "+String(aiv)+" RH[%]: "+String(rh));
  */
  int t1=millis();
  if(t1-t0<1000){delay(1000-t1+t0);}

}