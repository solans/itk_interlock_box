/**
 * MOQCHI: Module QC Hardware Interlock
 * EP-ADE-TK
 * 
 * Carlos.Solans@cern.ch
 **/
#include <Arduino.h>
#include <mbed.h>
#include <Wire.h>
#include <Arduino_MachineControl.h>
using namespace machinecontrol;

mbed::PwmOut AO_0(PJ_11);
mbed::PwmOut AO_1(PK_1);

float calibT(float R, float Beta, float R0, float T0){
  return 1./((1./T0) + (1./Beta)*log(R/R0) );
}

float calibHIH(float Vout, float Vin, float T=25){
  return (((Vout/Vin)-0.16)/0.0062)/(1.0546-0.002184*T);
}
bool triggered=false;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  analogReadResolution(16);
  
  digital_inputs.init();
  temp_probes.tc.begin();
  temp_probes.enableTC();
  AO_0.period_ms(2);
  AO_0.write(5.1/10.5);  
  AO_1.period_ms(2);
  AO_1.write(5.1/10.5);
  analog_in.set0_10V();
}

void loop() {

  int t0=millis();
  
  //Door open
  float door_open = digital_inputs.read(DIN_READ_CH_PIN_00);
  Serial.println("Door status: "+String(door_open));
  bool di_door_open = (door_open==0?true:false);
  if(di_door_open){Serial.println("Door open");}
  
  //Box temperature
  temp_probes.selectChannel(0);
  float box_temp = temp_probes.tc.readTemperature();
  Serial.println("Box temperature [C]: "+String(box_temp));
  bool di_box_temp = (box_temp>30?true:false);
  if(di_box_temp){Serial.println("Box temperature above threshold");}
  
  //Module temperature
  int raw = analogRead(PC_3C);
  float volt = (raw * 3.0) / 65535 / 0.28057;
  float ohm = 10000*((5./volt)-1);
  float mod_temp = calibT(ohm,3892,10000,298.15)-273.15;
  Serial.println("Module Temperature [C]: "+String(mod_temp));
  //Serial.println("0 raw: "+String(raw)+", volt: "+String(volt)+", ohm: "+String(ohm)+", temp: "+String(temp));
  bool di_mod_temp = (mod_temp>30?true:false);
  if(di_mod_temp){Serial.println("Module temperature above threshold");}
  
  //Box humidity
  raw = analogRead(PC_2C);
  volt = (raw * 3.0) / 65535 / 0.28057;
  ohm = 10000*((5./volt)-1);
  float box_humi = calibHIH(volt,5.0,box_temp);
  Serial.println("Box humidity [%]: "+String(box_humi));
  //Serial.println("1 raw: "+String(raw)+", volt: "+String(volt)+", ohm: "+String(ohm)+", rh: "+String(rh));
  bool di_box_humi = (box_humi>50?true:false);
  if(di_box_humi){Serial.println("Box humidity above threshold");}
  
  //trigger the interlock
  if(di_door_open || di_box_temp || di_mod_temp || di_box_humi){
    Serial.println("Action triggered");
    digital_outputs.set(0,false);
    digital_outputs.set(1,false);
  }else{
    Serial.println("Action restored");
    digital_outputs.set(0,true);
    digital_outputs.set(1,true);  
  }

  //wait one second
  int t1=millis();
  if(t1-t0<1000){delay(1000-t1+t0);}

}