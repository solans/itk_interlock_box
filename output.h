#ifndef OUTPUT
#define OUTPUT

class output{

public:
  
  output(){};
  virtual ~output(){};
  virtual void init()=0;
  virtual void trigger()=0;
  virtual void acknowledge()=0;
  virtual bool eval()=0;

};
#endif
