#include "tc_input.h"
#include <Arduino_MachineControl.h>
using namespace machinecontrol;

tc_input::tc_input(String name, int ch, float threshold, bool trigger_above){
    m_name = name;
    m_ch = ch;
    m_threshold = threshold;
    m_trigger_above = trigger_above;
    m_value = 0;
    m_triggered = false;
}

tc_input::~tc_input(){}

String tc_input::get_name(){
  return m_name;
}

void tc_input::init(){
    temp_probes.tc.begin();
    temp_probes.enableTC();
}

float tc_input::value(){
  temp_probes.selectChannel(m_ch);
  m_value = temp_probes.tc.readTemperature();
  Serial.println(m_name+" [C]: "+String(m_value,3));
  return m_value;
}

bool tc_input::eval(){
  value();
  if(m_trigger_above && m_value > m_threshold){m_triggered=true;}
  else if(!m_trigger_above && m_value < m_threshold){m_triggered=true;}
  else{m_triggered=false;}
  return m_triggered;
}
