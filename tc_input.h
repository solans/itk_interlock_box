#ifndef TC_INPUT
#define TC_INPUT

#include <Arduino.h>
#include "input.h"
/**
 * Thermo-couple input
 **/

class tc_input: public input{

public:
  
  tc_input(String name, int ch, float threshold, bool trigger_above);
  ~tc_input();
  String get_name();
  float value();
  void init();
  bool eval();
  
private:
  String m_name;
  int m_ch;
  float m_value;
  float m_threshold;
  bool m_trigger_above;
  bool m_triggered;
};
#endif
